import 'package:flutter/material.dart';

class AppColors {
  static const Color orange = Color(0xffF87C58);
  static const Color ligthBlue = Color(0xff4E536D);
  static const Color darkBlue = Color(0xFF333647);
}
