import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:portfolio/core/menu/data/pod/menu_pod.dart';
import 'package:portfolio/features/portfolio_viewer/data/pod/portfolio_pod.dart';
import 'package:portfolio/features/portfolio_viewer/data/pod/portfolio_state.dart';
import 'package:portfolio/features/portfolio_viewer/data/repository/portfolio_repository.dart';

final menuPod = StateNotifierProvider<MenuPod, MenuState>(
  (ref) {
    return MenuPod();
  },
);

final portfolioPod = StateNotifierProvider<PortfolioPod, PortfolioState>(
  (ref) {
    return PortfolioPod(PortfolioRepository());
  },
);
