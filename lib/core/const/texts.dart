class Texts {
  static const String welcomeText = "Hi,";
  static const String iam = "I'm Dezső Csete";
  static const String title = "Flutter Developer";
  static const String description =
      "I'm a Hungarian Flutter Developer with 3 years of experience. My main profile is making Cross Platform Mobile Applications, and highly responsive Web pages, using Flutter as Frontend and Firebase as a backend.";

  static const String fullAboutMeDescription = '''🔥 Mobile Developer / Flutter Developer / Freelance Projects 🔥

I create mobile applications as well with Flutter. I have a strong understanding of the Dart language, and I feel very comfortable writting applications with it and also I am invested in the language, have created some open source packages for it, have been learning it every day.

Lately I have been Freelancing with Flutter, with a Firebase backend. Created numerous mobile applications and web applications. My passion is with the Google Dev guys, who have created a really special tool for developers like me.
''';
}
