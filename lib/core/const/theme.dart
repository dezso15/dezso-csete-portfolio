import 'package:flutter/material.dart';
import 'package:portfolio/core/const/colors.dart';

final portfolioThemeData = ThemeData(
  textTheme: TextTheme(
    headline1: const TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
    ),
    bodyText1: const TextStyle(fontSize: 16.0),
    headline2: TextStyle(
      fontSize: 20,
      color: Colors.white.withOpacity(0.89),
      fontWeight: FontWeight.bold,
    ),
    button: const TextStyle(
      fontSize: 18.0,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
  ),
  cardTheme: CardTheme(
    elevation: 3,
    color: const Color(0xFF26304b),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15.0),
    ),
  ),
  primaryIconTheme: const IconThemeData(
    color: AppColors.ligthBlue,
    size: 30,
  ),
  backgroundColor: AppColors.darkBlue,
  iconTheme: const IconThemeData(
    color: Color(0xFF505e85),
    size: 30,
  ),
  appBarTheme: const AppBarTheme(
    color: Color(0xFF333647),
  ),
);
