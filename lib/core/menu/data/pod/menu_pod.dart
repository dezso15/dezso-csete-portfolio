import 'package:flutter_riverpod/flutter_riverpod.dart';

enum MenuState {
  landingPage,
  portfolioViewer,
  cvViewer,
  contactMe,
}

class MenuPod extends StateNotifier<MenuState> {
  MenuPod() : super(MenuState.landingPage);

  void changePage(MenuState newState) {
    state = newState;
  }
}
