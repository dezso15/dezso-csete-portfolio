import 'package:flutter/material.dart';
import 'package:portfolio/core/menu/presentation/widgets/portfolio_appbar.dart';
import 'package:portfolio/core/menu/presentation/widgets/portfolio_background.dart';
import 'package:responsive_framework/responsive_framework.dart';

class Menu extends StatelessWidget {
  final Widget child;
  final bool noDezso;
  Menu({required this.child, this.noDezso = false});

  @override
  Widget build(BuildContext context) {
    return ResponsiveWrapper.builder(
      Scaffold(
        body: Stack(
          children: [
            Column(
              children: [
                const SizedBox(height: 100),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height - 100,
                  child: PortfolioBackground(child: child),
                ),
              ],
            ),
            PortfolioAppBar(noDezso),
          ],
        ),
      ),
      debugLog: true,
      maxWidth: 1920,
      minWidth: 2,
      defaultScale: true,
      breakpoints: [
        ResponsiveBreakpoint.resize(2, name: MOBILE),
        ResponsiveBreakpoint.resize(800, name: TABLET),
        ResponsiveBreakpoint.resize(1920, name: DESKTOP),
      ],
      background: Container(
        color: Theme.of(context).backgroundColor,
      ),
    );
  }
}
