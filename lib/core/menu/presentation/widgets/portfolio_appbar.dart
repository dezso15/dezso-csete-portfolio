import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:portfolio/core/const/pods.dart';
import 'package:portfolio/core/menu/data/pod/menu_pod.dart';
import 'package:portfolio/core/router/router.gr.dart';

const _iconSize = 150.0;
const _endPoint = 65.0;

class PortfolioAppBar extends StatelessWidget {
  final bool noDezso;

  PortfolioAppBar(this.noDezso);

  @override
  Widget build(BuildContext context) {
    const menuButtonsWidth = 450.0;
    return LayoutBuilder(
      builder: (ctx, constraints) {
        final deviceWidth = MediaQuery.of(ctx).size.width - 20;

        double offsetHorizontal = (deviceWidth) / 2 - (_iconSize / 2);
        double offsetVertical = 0;
        if (offsetHorizontal < menuButtonsWidth + 30) {
          offsetVertical = (menuButtonsWidth + 30 - offsetHorizontal);
        }
        offsetVertical = min(_endPoint, offsetVertical);
        if (deviceWidth < 500) {
          return const _MobileAppBar();
        }
        return _DesktopAppBar(
          noDezso: noDezso,
          offsetHorizontal: offsetHorizontal,
          offsetVertical: offsetVertical,
        );
      },
    );
  }
}

class _DesktopAppBar extends StatelessWidget {
  final double offsetHorizontal;
  final double offsetVertical;
  final bool noDezso;
  const _DesktopAppBar({
    Key? key,
    required this.offsetHorizontal,
    required this.offsetVertical,
    required this.noDezso,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Material(
          elevation: 0,
          child: Container(
            width: double.infinity,
            height: 100,
            color: Colors.transparent,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              color: Theme.of(context).appBarTheme.backgroundColor,
              height: 100,
              child: _MenuRow(),
            ),
          ),
        ),
        if (!noDezso)
          Positioned(
            left: offsetHorizontal,
            top: 20 + offsetVertical,
            child: _Avatar(
              iconSize: _iconSize,
            ),
          ),
        Container(
          height: 235,
        ),
      ],
    );
  }
}

class _MenuButton extends ConsumerWidget {
  final String text;
  final MenuState option;
  final bool isInFirstColumn;

  const _MenuButton({
    required this.text,
    required this.option,
    required this.isInFirstColumn,
  });

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final menuState = watch(menuPod);
    final menu = watch(menuPod.notifier);
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: GestureDetector(
          onTap: () {
            menu.changePage(option);

            switch (option) {
              case MenuState.landingPage:
                AutoRouter.of(context).push(const LandingPageRoute());
                break;
              case MenuState.portfolioViewer:
                AutoRouter.of(context).push(const PortfolioPageRoute());
                break;
              case MenuState.cvViewer:
                AutoRouter.of(context).push(const CVViewerPageRoute());
                break;
              case MenuState.contactMe:
                AutoRouter.of(context).push(const ContactMePageRoute());
                break;
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                text,
                style: Theme.of(context).textTheme.headline2,
                textAlign: isInFirstColumn ? TextAlign.end : TextAlign.start,
              ),
              const SizedBox(
                height: 3,
              ),
              if (menuState == option)
                Container(
                  height: 2,
                  width: 20,
                  color: const Color(0xffF87C58),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

class _MobileAppBar extends StatelessWidget {
  const _MobileAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2,
      child: Container(
        height: 100,
        width: double.infinity,
        color: Theme.of(context).appBarTheme.backgroundColor,
        child: _MenuRow(),
      ),
    );
  }
}

class _MenuRow extends StatelessWidget {
  const _MenuRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 2,
          child: Wrap(
            spacing: 10,
            runSpacing: 10,
            alignment: WrapAlignment.end,
            runAlignment: WrapAlignment.end,
            children: const [
              _MenuButton(
                text: "Home",
                option: MenuState.landingPage,
                isInFirstColumn: true,
              ),
              _MenuButton(
                text: "My Portfolio",
                option: MenuState.portfolioViewer,
                isInFirstColumn: true,
              ),
            ],
          ),
        ),
        const Spacer(flex: 1),
        Expanded(
          flex: 2,
          child: Wrap(
            spacing: 10,
            runSpacing: 10,
            alignment: WrapAlignment.start,
            children: const [
              _MenuButton(
                text: "My CV",
                option: MenuState.cvViewer,
                isInFirstColumn: false,
              ),
              _MenuButton(
                text: "Contact Me",
                option: MenuState.contactMe,
                isInFirstColumn: false,
              ),
            ],
          ),
        )
      ],
    );
  }
}

class _Avatar extends StatelessWidget {
  final double iconSize;

  _Avatar({required this.iconSize});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: iconSize,
      width: iconSize,
      color: Colors.transparent,
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        child: Image.asset("assets/images/enkivagva.png"),
      ),
    );
  }
}
