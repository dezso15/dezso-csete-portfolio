import 'package:flutter/material.dart';

class PortfolioBackground extends StatelessWidget {
  final Widget child;

  const PortfolioBackground({required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF333647),
      child: child,
    );
  }
}
