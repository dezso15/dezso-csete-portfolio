import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:portfolio/features/contact_me/presentation/page/contact_me_page.dart';
import 'package:portfolio/features/cv_viewer/presentation/page/cv_viewer_page.dart';
import 'package:portfolio/features/landing_page/presentation/page/landing_page.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/page/portfolio_page.dart';

@MaterialAutoRouter(
  routes: [
    CustomRoute(
      page: LandingPage,
      initial: true,
      path: "home",
      maintainState: false,
      transitionsBuilder: NoRouteAnimation.none,
    ),
    CustomRoute(
      page: PortfolioPage,
      initial: true,
      path: "portfolios",
      maintainState: false,
      transitionsBuilder: NoRouteAnimation.none,
    ),
    CustomRoute(
      page: CVViewerPage,
      initial: true,
      path: "cvviewer",
      maintainState: false,
      transitionsBuilder: NoRouteAnimation.none,
    ),
    CustomRoute(
      page: ContactMePage,
      initial: true,
      path: "contactme",
      maintainState: false,
      transitionsBuilder: NoRouteAnimation.none,
    ),
  ],
)
class $AppRouter {}

class NoRouteAnimation {
  static const RouteTransitionsBuilder none = _none;

  static Widget _none(
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: Offset.zero,
        end: Offset.zero,
      ).animate(animation),
      child: child,
    );
  }
}
