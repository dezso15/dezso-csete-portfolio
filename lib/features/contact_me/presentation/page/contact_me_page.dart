import 'package:flutter/material.dart';
import 'package:portfolio/core/menu/presentation/widgets/menu.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/contact_me.dart';

class ContactMePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Menu(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ContactMe(),
        ],
      ),
    );
  }
}
