import 'package:flutter/material.dart';
import 'package:portfolio/core/menu/presentation/widgets/menu.dart';
import 'package:portfolio/features/cv_viewer/presentation/widgets/pdf_viewer.dart';

class CVViewerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Menu(
      noDezso: true,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [PdfViewer.instance],
        ),
      ),
    );
  }
}
