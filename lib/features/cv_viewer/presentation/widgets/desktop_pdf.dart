import 'package:flutter/material.dart';
import 'package:portfolio/features/cv_viewer/presentation/widgets/pdf_viewer.dart';

class DesktopPdfView extends PdfViewer {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

PdfViewer getPdfViewer() => DesktopPdfView();
