import 'package:flutter/cupertino.dart';

import 'pdf_viewer_stub.dart' if (dart.library.io) 'desktop_pdf.dart' if (dart.library.js) 'web_pdf.dart';

abstract class PdfViewer extends StatelessWidget {
  static PdfViewer? _instance;

  static PdfViewer get instance {
    _instance ??= getPdfViewer();
    return _instance!;
  }
}
