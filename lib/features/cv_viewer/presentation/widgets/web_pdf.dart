import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'dart:html' as html;

import 'package:portfolio/features/cv_viewer/presentation/widgets/pdf_viewer.dart';

class WebPdfViewer extends PdfViewer {
  WebPdfViewer() {
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory('iframe', (int viewId) {
      var iframe = html.IFrameElement();
      iframe.src =
          'https://firebasestorage.googleapis.com/v0/b/foglalok-dcdfe.appspot.com/o/public%2FDezso_Csete_Flutter_Developer_CV.pdf?alt=media';
      return iframe;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height - 110,
      child: const HtmlElementView(viewType: 'iframe'),
    );
  }
}

PdfViewer getPdfViewer() => WebPdfViewer();
