import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio_link.dart';

part 'portfolio.freezed.dart';
part 'portfolio.g.dart';

enum AppPlatform {
  @JsonValue("MOBILE")
  MOBILE,
  @JsonValue("WEB")
  WEB
}

@freezed
class Portfolio with _$Portfolio {
  factory Portfolio({
    required String name,
    required List<PortfolioLink> links,
    required List<String> imagePaths,
    required final String description,
    required final String as,
    required final String teamDescription,
    required final String developmentYear,
    required final AppPlatform platform,
  }) = _Portfolio;

  factory Portfolio.fromJson(Map<String, dynamic> json) => _$PortfolioFromJson(json);
}
