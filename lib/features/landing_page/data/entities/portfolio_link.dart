import 'package:freezed_annotation/freezed_annotation.dart';

part 'portfolio_link.freezed.dart';
part 'portfolio_link.g.dart';

@freezed
class PortfolioLink with _$PortfolioLink {
  factory PortfolioLink({
    required String link,
    required String iconPath,
  }) = _PortfolioLink;

  factory PortfolioLink.fromJson(Map<String, dynamic> json) => _$PortfolioLinkFromJson(json);
}
