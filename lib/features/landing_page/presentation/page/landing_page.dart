import 'package:flutter/material.dart';
import 'package:portfolio/core/menu/presentation/widgets/menu.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/about_me.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/contact_me.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/introduction.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/skills.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/social.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_viewer.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Menu(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 110,
            ),
            Introduction(),
            const SizedBox(height: 50),
            Skills(isLight: true),
            const SizedBox(height: 50),
            AboutMe(isLight: false),
            const SizedBox(height: 50),
            PortfoilioViewer(
              isLight: true,
              page: PortfolioViewerOnPage.landingPage,
            ),
            const SizedBox(height: 50),
            Social(isLight: false),
            const SizedBox(height: 50),
            ContactMe(isLight: false),
          ],
        ),
      ),
    );
  }
}
