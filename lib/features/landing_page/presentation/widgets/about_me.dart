import 'package:flutter/material.dart';
import 'package:portfolio/core/const/colors.dart';
import 'package:portfolio/core/const/texts.dart';

class AboutMe extends StatelessWidget {
  final bool isLight;
  AboutMe({this.isLight = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      color: isLight ? AppColors.ligthBlue : AppColors.darkBlue,
      width: double.infinity,
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SizedBox(height: 20),
          Wrap(
            runSpacing: 45,
            spacing: 250,
            alignment: WrapAlignment.center,
            children: [
              CircleAvatar(
                radius: 138,
                backgroundColor: AppColors.orange,
                child: CircleAvatar(
                  child: Image.asset(
                    "assets/images/en.png",
                    width: 260,
                  ),
                  radius: 134,
                  backgroundColor: AppColors.darkBlue,
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SizedBox(
                  width: 800,
                  child: SelectableText(
                    Texts.fullAboutMeDescription,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
