import 'package:flutter/material.dart';
import 'package:mailto/mailto.dart';
import 'package:portfolio/core/const/colors.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactMe extends StatelessWidget {
  final bool isLight;
  ContactMe({this.isLight = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: isLight ? AppColors.ligthBlue : AppColors.darkBlue,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 50),
        child: Center(
          child: InkWell(
            child: Card(
              color: AppColors.orange,
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Text(
                  "Contact Me",
                  style: Theme.of(context).textTheme.headline2!.copyWith(fontSize: 26),
                ),
              ),
            ),
            onTap: () {
              _sendMailToMe();
            },
          ),
        ),
      ),
    );
  }

  Future<void> _sendMailToMe() async {
    final mailtoLink = Mailto(
      to: ['dezso15@gmail.com'],
      cc: [],
      subject: 'Email from Portfolio Page',
      body: '',
    );
    await launch('$mailtoLink');
  }
}
