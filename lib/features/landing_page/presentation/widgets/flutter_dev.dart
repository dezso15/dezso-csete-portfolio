import 'package:flutter/material.dart';
import 'package:portfolio/core/const/texts.dart';

class FlutterDev extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          SelectableText(
            Texts.welcomeText,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 28,
            ),
          ),
          SizedBox(height: 2),
          SelectableText(
            Texts.iam,
            style: TextStyle(
              color: Colors.white,
              fontSize: 42,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 2),
          SelectableText(
            Texts.title,
            style: TextStyle(
              color: Color(0xffF87C58),
              fontSize: 32,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 4),
          SizedBox(
            width: 800,
            child: SelectableText(
              Texts.description,
              textAlign: TextAlign.justify,
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
