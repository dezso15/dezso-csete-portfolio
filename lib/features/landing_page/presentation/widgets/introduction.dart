import 'package:flutter/material.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/flutter_dev.dart';

class Introduction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        verticalDirection: VerticalDirection.up,
        runAlignment: WrapAlignment.end,
        spacing: 180,
        runSpacing: 30,
        children: [
          FlutterDev(),
          SizedBox(width: 550, child: Image.asset("assets/images/asztal.png")),
        ],
      ),
    );
  }
}
