import 'package:flutter/material.dart';
import 'package:portfolio/core/const/colors.dart';
import 'package:portfolio/core/widgets/section_title.dart';

class Skills extends StatelessWidget {
  final bool isLight;
  Skills({this.isLight = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 40),
      width: double.infinity,
      color: isLight ? AppColors.ligthBlue : AppColors.darkBlue,
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SectionTitle(title: "My Skills"),
          const SizedBox(height: 50),
          Wrap(
            runSpacing: 45,
            spacing: 250,
            alignment: WrapAlignment.center,
            children: const [
              SkillIcon(
                iconPath: "assets/images/dart.png",
                text: "Dart",
              ),
              SkillIcon(
                iconPath: "assets/images/flutter.png",
                text: "Flutter",
              ),
              SkillIcon(
                iconPath: "assets/images/firebase.png",
                text: "Firebase",
              ),
              SkillIcon(
                iconPath: "assets/images/git.png",
                text: "Git",
              ),
            ],
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}

class SkillIcon extends StatelessWidget {
  final String text;
  final String iconPath;

  const SkillIcon({required this.text, required this.iconPath});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          iconPath,
          width: 170,
          height: 170,
        ),
        const SizedBox(height: 8),
        SelectableText(
          text,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 26,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
