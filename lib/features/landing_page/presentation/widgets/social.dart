import 'package:flutter/material.dart';
import 'package:portfolio/core/const/colors.dart';
import 'package:portfolio/core/widgets/section_title.dart';
import 'package:url_launcher/url_launcher.dart';

class Social extends StatelessWidget {
  final bool isLight;
  Social({this.isLight = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 40),
      width: double.infinity,
      color: isLight ? AppColors.ligthBlue : AppColors.darkBlue,
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SectionTitle(title: "Social Links"),
          const SizedBox(height: 50),
          Wrap(
            runSpacing: 45,
            spacing: 250,
            alignment: WrapAlignment.center,
            children: [
              _SocialIcon(
                iconPath: "assets/images/linkedin.png",
                link: "https://www.linkedin.com/in/dezso-csete-31a974152/",
                name: "Connect on LinkedIn",
              ),
              _SocialIcon(
                iconPath: "assets/images/wordpress.png",
                link: "https://hobbister.com/",
                name: "Follow my blog",
              ),
            ],
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}

class _SocialIcon extends StatelessWidget {
  final String name;
  final String link;
  final String iconPath;

  _SocialIcon({required this.name, required this.link, required this.iconPath});

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () {
          _launchURL(link);
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              iconPath,
              height: 50,
            ),
            const SizedBox(width: 10),
            Text(
              name,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 26,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
