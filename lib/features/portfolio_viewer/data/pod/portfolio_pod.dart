import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio.dart';
import 'package:portfolio/features/portfolio_viewer/data/pod/portfolio_state.dart';
import 'package:portfolio/features/portfolio_viewer/data/repository/portfolio_repository.dart';

class PortfolioPod extends StateNotifier<PortfolioState> {
  final PortfolioRepository _portfolioRepository;

  PortfolioPod(this._portfolioRepository) : super(const PortfolioState.loading());

  Future<List<Portfolio>> getPortfolios(BuildContext context) async {
    final portfolios = await _portfolioRepository.getPortolios(context);

    state = PortfolioState.loaded(portfolios);

    return portfolios;
  }
}
