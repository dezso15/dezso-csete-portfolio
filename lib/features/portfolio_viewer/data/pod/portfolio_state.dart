import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio.dart';

part 'portfolio_state.freezed.dart';

@freezed
abstract class PortfolioState with _$PortfolioState {
  const factory PortfolioState.loading() = _InitState;
  const factory PortfolioState.loaded(List<Portfolio> portfolios) = _LoadedState;
}
