import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio.dart';

class PortfolioRepository {
  Future<List<Portfolio>> getPortolios(BuildContext context) async {
    String data = await DefaultAssetBundle.of(context).loadString("assets/portfolios.json");
    final jsonResult = jsonDecode(data);

    final portfolios =
        (jsonResult["portfolios"] as List<dynamic>).map((portfolioJson) => Portfolio.fromJson(portfolioJson)).toList();

    return portfolios;
  }
}
