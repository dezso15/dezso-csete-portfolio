import 'package:flutter/material.dart';
import 'package:portfolio/core/menu/presentation/widgets/menu.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/contact_me.dart';
import 'package:portfolio/features/landing_page/presentation/widgets/social.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/package_list.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_viewer.dart';

class PortfolioPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Menu(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 30),
            PortfoilioViewer(
              isLight: false,
              page: PortfolioViewerOnPage.portfolioPage,
            ),
            const SizedBox(height: 50),
            const PackageList(),
            const SizedBox(height: 50),
            Social(isLight: true),
            ContactMe(isLight: true),
          ],
        ),
      ),
    );
  }
}
