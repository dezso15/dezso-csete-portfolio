import 'package:flutter/material.dart';

class CarouselArrowIcon extends StatelessWidget {
  final IconData arrowIcon;
  final Function onClick;

  CarouselArrowIcon({required this.arrowIcon, required this.onClick});

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () {
          onClick();
        },
        child: Icon(
          arrowIcon,
          color: Colors.white,
        ),
      ),
    );
  }
}
