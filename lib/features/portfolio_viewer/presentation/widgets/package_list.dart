import 'package:flutter/material.dart';
import 'package:portfolio/core/widgets/section_title.dart';
import 'package:url_launcher/url_launcher.dart';

class PackageList extends StatelessWidget {
  const PackageList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          const SizedBox(height: 35),
          const SectionTitle(title: "My Pub.dev Contributions"),
          const SizedBox(height: 50),
          SizedBox(
            width: 1000,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                _PackageLink(
                  name: "smooth_scroll_web",
                  link: "https://pub.dev/packages/smooth_scroll_web",
                ),
                SizedBox(height: 10),
                _PackageLink(
                  name: "webscrollbar",
                  link: "https://pub.dev/packages/webscrollbar",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _PackageLink extends StatelessWidget {
  final String name;
  final String link;
  const _PackageLink({required this.name, required this.link, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () {
          launch(link);
        },
        child: Text(
          name,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.blue,
                decoration: TextDecoration.underline,
              ),
        ),
      ),
    );
  }
}
