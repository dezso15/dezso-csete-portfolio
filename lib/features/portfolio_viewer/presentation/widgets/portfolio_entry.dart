import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:portfolio/core/const/colors.dart';
import 'package:portfolio/core/const/pods.dart';
import 'package:portfolio/core/menu/data/pod/menu_pod.dart';
import 'package:portfolio/core/router/router.gr.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_image_viewer.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_link_widget.dart';

abstract class PortfolioEntry extends Widget {
  factory PortfolioEntry.portfolioPage(Portfolio portfolio) = _PortfolioPage;
  factory PortfolioEntry.landingPage(Portfolio portfolio) = _LandingPage;
}

class _PortfolioPage extends ConsumerWidget implements PortfolioEntry {
  final Portfolio portfolio;
  _PortfolioPage(this.portfolio);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SelectableText(
          portfolio.name,
          textAlign: TextAlign.justify,
          style: const TextStyle(
            color: AppColors.orange,
            fontWeight: FontWeight.bold,
            fontSize: 50,
          ),
        ),
        PortfolioImageViewer(portfolio),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SizedBox(
            width: 1000,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 14),
                SelectableText(
                  "Team Status: " + portfolio.teamDescription,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(height: 4),
                SelectableText(
                  "As: " + portfolio.as,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(height: 4),
                SelectableText(
                  "Year: " + portfolio.developmentYear,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(height: 8),
                SelectableText(
                  portfolio.description,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                  ),
                ),
                const SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    for (final link in portfolio.links) PortfolioLinkWidget(link),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _LandingPage extends ConsumerWidget implements PortfolioEntry {
  final Portfolio portfolio;
  _LandingPage(this.portfolio);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Wrap(
      runSpacing: 45,
      spacing: 20,
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        if (portfolio.platform == AppPlatform.MOBILE)
          SizedBox(height: 600, child: Image.asset(portfolio.imagePaths.first))
        else
          SizedBox(width: 700, child: Image.asset(portfolio.imagePaths.first)),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: SizedBox(
            width: 800,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SelectableText(
                  portfolio.name,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                  ),
                ),
                const SizedBox(height: 4),
                SelectableText(
                  "Team Status: " + portfolio.teamDescription,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(height: 4),
                SelectableText(
                  "As: " + portfolio.as,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(height: 4),
                SelectableText(
                  "Year: " + portfolio.developmentYear,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(height: 8),
                SelectableText(
                  portfolio.description,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                  ),
                ),
                const SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    for (final link in portfolio.links) PortfolioLinkWidget(link),
                  ],
                ),
                const SizedBox(height: 12),
                MouseRegion(
                  cursor: SystemMouseCursors.click,
                  child: GestureDetector(
                    onTap: () {
                      watch(menuPod.notifier).changePage(MenuState.portfolioViewer);
                      AutoRouter.of(context).push(const PortfolioPageRoute());
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 10,
                      ),
                      color: AppColors.orange,
                      child: const Text(
                        "Go to Portfolios",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
