import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio.dart';

class PortfolioImageViewer extends StatelessWidget {
  final Portfolio portfolio;
  PortfolioImageViewer(this.portfolio);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 700,
      width: MediaQuery.of(context).size.width * 0.95,
      child: Swiper(
        scale: 0.9,
        itemHeight: 700,
        itemWidth: portfolio.platform == AppPlatform.MOBILE ? 400 : MediaQuery.of(context).size.width * 0.8,
        layout: SwiperLayout.TINDER,
        viewportFraction: 0.4,
        itemBuilder: (BuildContext context, int index) {
          return Image.asset(portfolio.imagePaths[index]);
        },
        itemCount: portfolio.imagePaths.length,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
      ),
    );
  }
}
