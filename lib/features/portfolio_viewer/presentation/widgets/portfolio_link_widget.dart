import 'package:flutter/material.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio_link.dart';
import 'package:url_launcher/url_launcher.dart';

class PortfolioLinkWidget extends StatelessWidget {
  final PortfolioLink link;
  PortfolioLinkWidget(this.link);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () {
          launch(link.link);
        },
        child: Image.asset(
          link.iconPath,
          width: 75,
        ),
      ),
    );
  }
}
