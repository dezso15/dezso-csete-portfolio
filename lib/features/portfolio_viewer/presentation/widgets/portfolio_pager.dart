import 'package:flutter/material.dart';
import 'package:portfolio/features/landing_page/data/entities/portfolio.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_entry.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_viewer.dart';

class PortfolioPager extends StatefulWidget {
  final List<Portfolio> portfolios;
  final PortfolioViewerOnPage page;

  PortfolioPager({required this.portfolios, required key, required this.page}) : super(key: key);

  @override
  PortfolioPagerState createState() => PortfolioPagerState();
}

class PortfolioPagerState extends State<PortfolioPager> {
  int index = 0;

  void next() {
    setState(() {
      index++;
      if (index > widget.portfolios.length - 1) {
        index = 0;
      }
    });
  }

  void previous() {
    setState(() {
      index--;
      if (index < 0) {
        index = widget.portfolios.length - 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (dragDetails) {
        if (dragDetails.primaryVelocity != null) {
          if (dragDetails.primaryVelocity! > 1000.0) {
            if (widget.page == PortfolioViewerOnPage.landingPage) {
              previous();
            }
          } else if (dragDetails.primaryVelocity! < -1000.0) {
            if (widget.page == PortfolioViewerOnPage.landingPage) {
              next();
            }
          }
        }
      },
      child: _getPortfolioEntry(widget.portfolios[index]),
    );
  }

  Widget _getPortfolioEntry(Portfolio portfolio) {
    if (widget.page == PortfolioViewerOnPage.landingPage) {
      return PortfolioEntry.landingPage(portfolio);
    } else {
      return PortfolioEntry.portfolioPage(portfolio);
    }
  }
}
