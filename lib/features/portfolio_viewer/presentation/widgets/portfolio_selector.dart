import 'package:flutter/material.dart';
import 'package:portfolio/core/const/colors.dart';

class PortfolioSelector extends StatelessWidget {
  final bool isLight;
  PortfolioSelector({required this.isLight});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: isLight ? AppColors.ligthBlue : AppColors.darkBlue,
      width: double.infinity,
    );
  }
}
