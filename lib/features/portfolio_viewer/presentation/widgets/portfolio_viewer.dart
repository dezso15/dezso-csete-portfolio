import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:portfolio/core/const/colors.dart';
import 'package:portfolio/core/const/pods.dart';
import 'package:portfolio/core/widgets/section_title.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/carousel_arrow_icon.dart';
import 'package:portfolio/features/portfolio_viewer/presentation/widgets/portfolio_pager.dart';

enum PortfolioViewerOnPage {
  portfolioPage,
  landingPage,
}

class PortfoilioViewer extends StatelessWidget {
  final bool isLight;
  final PortfolioViewerOnPage page;
  final GlobalKey<PortfolioPagerState> carouselController = GlobalKey<PortfolioPagerState>();

  PortfoilioViewer({this.isLight = false, required this.page});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 35),
      color: isLight ? AppColors.ligthBlue : AppColors.darkBlue,
      width: double.infinity,
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CarouselArrowIcon(
                onClick: () {
                  carouselController.currentState?.previous();
                },
                arrowIcon: Icons.arrow_back_ios,
              ),
              const SizedBox(width: 25),
              const SectionTitle(title: "My Portfolio"),
              const SizedBox(width: 25),
              CarouselArrowIcon(
                onClick: () {
                  carouselController.currentState?.next();
                },
                arrowIcon: Icons.arrow_forward_ios,
              ),
            ],
          ),
          const SizedBox(height: 30),
          Consumer(
            builder: (ctx, watch, _) {
              final portfolioState = watch(portfolioPod);
              return portfolioState.when(
                loaded: (portfolios) {
                  return PortfolioPager(
                    portfolios: portfolios,
                    key: carouselController,
                    page: page,
                  );
                },
                loading: () {
                  watch(portfolioPod.notifier).getPortfolios(context);
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                },
              );
            },
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
